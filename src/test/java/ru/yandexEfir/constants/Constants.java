package ru.yandexEfir.constants;

public class Constants {
    public static final String yandexURL = "https://yandex.ru/";
    public static final String efirURL = yandexURL + "efir";
    public static final String allServicesURL = yandexURL + "all";

    public static final String efirLink = "[href=\"https://yandex.ru/efir?utm_source=yamain&utm_medium=allservices&utm_campaign=general_ru_desktop_no_all&from_block=morda_allservices_default_general_desktop\"]";
}
